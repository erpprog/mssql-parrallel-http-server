import { readFileSync, appendFileSync } from 'fs';
import { ConnectionPool, IRecordSet, IResult, IRow } from 'mssql'
import { ServerHttp2Stream } from 'http2';


export class Querier {
  constructor(strQ: string) {
    this.resultTable = []
    this.strQ = strQ
  }

  private servers: any[]
  private strQ: string
  private resultTable: any[] = []
  private stream: ServerHttp2Stream

  public setHttp2Stream(stream: ServerHttp2Stream) {
    this.stream = stream
  }
  async executeSQL(connectionString: string, strQ: string | TemplateStringsArray): Promise<IResult<IRow>> {
    // try {
    let pool: ConnectionPool
    let conn: ConnectionPool
    let result: IResult<any>

    const q: TemplateStringsArray = strQ as any
    try {
      pool = await new ConnectionPool(connectionString)
      conn = await pool.connect()
      result = await conn.query(q)
    } catch (e) {
      return e
    }
    return result
  }

  async processing(): Promise<any> {

    // const r = await Promise.all(
    let r = this.servers.map(async (server) => {
      let ob: any
      try {
        const result = await this.executeSQL("mssql://sa:ser09l@" + server.insance + "/sup_kkm", this.strQ)
        const recordset: IRecordSet<any> | Error = result.recordset
        if (this.resultTable.length === 0) {
          this.resultTable = [recordset]
        }
        if (recordset == undefined) { ob = [{ ib: server.code }] } else {
          ob = recordset.map((el) => {
            el.ib = server.code
            let ib_host: string = server.host.replace(/ost/, 'serv')
            el.ib_href = `http://${ib_host}:85/sup_kkm`
            return el
          })
        }
        this.resultTable = this.resultTable.concat([...ob])
      } catch (err) {
        ob = [{ ib: server.host, err }]
        this.resultTable = this.resultTable.concat([...ob])
      }
      return ob
    })
    return r
  }

  async execQueries(tt:string[] = undefined) {
    this.servers = this.servers || JSON.parse(readFileSync('config\/servers.json').toString())
    if (tt != undefined) {
      this.servers = this.servers.filter(e => {
        return tt.includes(e.code)        
      })
    }
    console.log('strQ = ' + this.strQ)
    const f = await this.processing()
    var result: any[] = []
    for await (const obj of f) {
      let str = ""
      try {
        if (obj.length) {
          result.push(obj)

          if (this.stream) {
            this.stream.write(`<span>${str}</span>`)
          }
        }

      } catch (e) {
        console.log(e)
        console.log(obj)
      }
    }
    let gt = []
    gt.sort((a, b) => { return (a.ib > b.ib ? 1 : 0) })
    //console.table(this.resultTable)
    if (this.stream) {
      this.stream.write(JSON.stringify(this.resultTable))
      this.stream.end()
      this.stream.close(200)
    }
    return result
  }
}
