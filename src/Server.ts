import { readFileSync } from 'fs';
import * as net from 'net'
import * as http from 'http';
import { Querier } from "./queryier";
import { TLSSocket } from 'tls';
import { METHODS } from 'http';
import * as url from 'url';
import { Stream } from 'stream';
import { SSL_OP_TLS_BLOCK_PADDING_BUG } from 'constants';
export class Server {
    constructor() {
        this.init()
    }
    public server: http.Server;
    q: any

    stream: Stream
    headers: http.IncomingHttpHeaders;

    public async requestHandler(req: http.IncomingMessage, res: http.ServerResponse) {





        req.setEncoding('utf8')

        if (req.method == 'GET') {
            let usp = new url.URLSearchParams(req.url.replace(/(.*)\?/, ``))
            let query = usp.get('SQL')

            if (req.method == 'GET' && query) {



                if (query) {
                    let r = await this.executeQuery(query)
                    console.log(r)

                    res.end(JSON.stringify(r))
                }
                else {
                    res.end(
                        readFileSync('src\/query.html')
                    )
                }
            }
        }
        req.on('data', async (data) => {
            console.log('data ' + data)


            let queryOpts = JSON.parse(data)

            console.log(queryOpts)
            let query = queryOpts.query;
            let tt = queryOpts.tt;
            console.log("query " + query)
             res.statusCode = 200;
             let r = await this.executeQuery(query, tt)
             console.log(r)
             res.end(JSON.stringify(r))


        })
        // this.executeQuery('select 1')
    }

    private init() {

        this.server = http.createServer();

        this.server.on('request', (req, res) => this.requestHandler(req, res))

        this.server.on('data', (data) => { console.log('data:' + data) })

        var t = this

        let port = 8810
        
        try{
            this.server.listen(port).on('error', (e) => {this.server.listen(port+1)});
        } catch {
            this.server.listen(port+1);
        }
        
        // this.server.listen(port, 'ws_it_23.cd.local', 2);
        console.log('\x1b[mStart')
        console.log(`listen ${JSON.stringify(this.server.address())}:${port}.`);
    }
    async executeQuery(q: string, tt?: string[]) {
        let qe = new Querier(q)
        
        return await qe.execQueries(tt)
    }
}
